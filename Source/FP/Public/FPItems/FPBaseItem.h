// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperSpriteActor.h"
#include "FPBaseItem.generated.h"

/**
 * 
 */
UCLASS()
class FP_API AFPBaseItem : public APaperSpriteActor
{
	GENERATED_BODY()
	
public:

	AFPBaseItem();

	UFUNCTION()

	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Collision")
	class USphereComponent* Collision;

};
