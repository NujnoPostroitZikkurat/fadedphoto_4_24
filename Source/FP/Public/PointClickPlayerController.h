// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PointClickPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class FP_API APointClickPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
protected:
	bool bCanTaken;

	virtual void PlayerTick(float DeltaTime) override;

	virtual void BeginPlay() override;
public:
	
	UFUNCTION(BlueprintCallable)
	UClass* GetActorUnderCoursoreClass();

	UFUNCTION(BlueprintCallable)
	AActor* GetActorUnderCoursore();
};
