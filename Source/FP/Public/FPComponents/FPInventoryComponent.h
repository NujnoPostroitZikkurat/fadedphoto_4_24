// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FP/Public/FPCoreTypes.h"
#include "FPInventoryComponent.generated.h"



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FP_API UFPInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UFPInventoryComponent();

	FOnItemTakenSignature OnItemTaken;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	class UClass* GetFirstItem();

	void GrabItem();

	void AddItem(class AFPBaseItem* _ITem, UClass* ItemClass);

	void DeleteItem(UClass* ItemClass);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
	TArray<FItemData> InventoryList;
private:
		
};
