// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "FPGameMode.h"
#include "FPCharacter.h"

AFPGameMode::AFPGameMode()
{
	// Set default pawn class to our character
	DefaultPawnClass = AFPCharacter::StaticClass();	
}
