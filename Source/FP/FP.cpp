// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "FP.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, FP, "FP" );
