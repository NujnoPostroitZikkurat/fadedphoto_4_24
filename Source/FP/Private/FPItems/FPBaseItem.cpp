// Fill out your copyright notice in the Description page of Project Settings.


#include "FP/Public/FPItems/FPBaseItem.h"
#include "Components/SphereComponent.h"

AFPBaseItem::AFPBaseItem() {
	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	//RootComponent = Collision;
}

void AFPBaseItem::BeginPlay()
{
	Super::BeginPlay();
}
