// Fill out your copyright notice in the Description page of Project Settings.


#include "FP/Public/PointClickPlayerController.h"

void APointClickPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void APointClickPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

UClass* APointClickPlayerController::GetActorUnderCoursoreClass()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	return Hit.GetActor()->GetClass();
}


AActor* APointClickPlayerController::GetActorUnderCoursore()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);
	return Hit.GetActor();
}
