// Fill out your copyright notice in the Description page of Project Settings.


#include "FP/Public/FPComponents/FPInventoryComponent.h"
#include "FP/Public/FPItems/FPBaseItem.h"
#include "GameFramework/Character.h"
#include "FP/Public/PointClickPlayerController.h"

// Sets default values for this component's properties
UFPInventoryComponent::UFPInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UFPInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

}

UClass* UFPInventoryComponent::GetFirstItem()
{
	if (GetWorld())
	{
		for (auto& i : InventoryList) {
			if (i.Count > 0) {
				//const auto S = *(i.ItemType->GetName());
				UE_LOG(LogTemp, Warning, TEXT("%s %i"), *i.ItemType->GetName(), i.Count);
				DeleteItem(i.ItemType);
				return i.ItemType;
			}
		}
	}
	return nullptr;
}

void UFPInventoryComponent::GrabItem()
{
	const auto Owner = Cast<ACharacter>(GetOwner());
	if (Owner->IsPlayerControlled()) {

		const auto PC = Cast<APointClickPlayerController>(Owner->GetController());

		auto ActorUnderCurClass = PC->GetActorUnderCoursoreClass();
		auto ActorUnderCur = PC->GetActorUnderCoursore();

		if (ActorUnderCurClass->IsChildOf(AFPBaseItem::StaticClass())) {

			auto Item = Cast<AFPBaseItem>(ActorUnderCur);

			if (!ActorUnderCur || !Item) return;

			AddItem(Item, ActorUnderCurClass);
		}
		return;
	}
	else return;
}

void UFPInventoryComponent::AddItem(AFPBaseItem* _ITem, UClass* ItemClass)
{
	if (!GetWorld()) return;

	for (auto& i : InventoryList) {
		if (i.ItemType == ItemClass) {
			i.Count++;
			_ITem->Destroy();
			//UE_LOG(LogTemp, Warning, TEXT("%s %i"), *i.ItemType->GetName(), i.Count);
			return;
		}
	}
	FItemData NewItem;
	NewItem.Count = 1;
	NewItem.ItemType = ItemClass;

	InventoryList.Add(NewItem);

	const auto Owner = GetOwner();
	if (Owner)
	{
		OnItemTaken.Broadcast(Owner);
	}

}

void UFPInventoryComponent::DeleteItem(UClass* ItemClass)
{
	for (auto& i : InventoryList) {
		if ((i.ItemType == ItemClass) && (i.Count >= 1)) {
			i.Count--;
			//UE_LOG(LogTemp, Warning, TEXT("%s %i"), *i.ItemType->GetName(), i.Count);
			if (i.Count == 0)
			{
					//InventoryList.Remove(i);
				return;
			}
		}
	}
	return;
}
